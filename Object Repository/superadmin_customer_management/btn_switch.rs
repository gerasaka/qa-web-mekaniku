<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_switch</name>
   <tag></tag>
   <elementGuidId>1b5c50af-c10a-4391-86c6-41a51889b58a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(), &quot;${customer_id}&quot;)][@class=&quot;mk-link&quot;]/ancestor::td/following-sibling::td/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(), &quot;${customer_id}&quot;)][@class=&quot;mk-link&quot;]/ancestor::td/following-sibling::td/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      <webElementGuid>8b1e6595-f345-42e6-a03a-13f1503dc9ba</webElementGuid>
   </webElementProperties>
</WebElementEntity>
