<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_option_car</name>
   <tag></tag>
   <elementGuidId>d6c52dc3-c886-4ce3-add9-11534dc439dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(), &quot;Mobil&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(), &quot;Mobil&quot;)]</value>
      <webElementGuid>625f29bc-ae14-4eea-a2ac-368a296ce05c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
