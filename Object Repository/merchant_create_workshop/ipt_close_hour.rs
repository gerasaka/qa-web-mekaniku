<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_close_hour</name>
   <tag></tag>
   <elementGuidId>275885a0-3248-44e6-8517-f4ffd169260f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(), &quot;Jam Tutup&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(), &quot;Jam Tutup&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[1]</value>
      <webElementGuid>4047f114-4144-4674-a991-03440dd525c6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
