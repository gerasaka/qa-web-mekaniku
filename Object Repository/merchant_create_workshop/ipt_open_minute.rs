<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_open_minute</name>
   <tag></tag>
   <elementGuidId>8b72ec61-4430-4de4-ab31-70ce14d18a23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(), &quot;Jam Buka&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(), &quot;Jam Buka&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[2]</value>
      <webElementGuid>a09d850f-1f30-4906-bf95-9a1f18456c63</webElementGuid>
   </webElementProperties>
</WebElementEntity>
