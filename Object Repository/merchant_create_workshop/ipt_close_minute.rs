<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_close_minute</name>
   <tag></tag>
   <elementGuidId>19ba9422-d6f7-4def-9082-507b866a14c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(), &quot;Jam Tutup&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(), &quot;Jam Tutup&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[2]</value>
      <webElementGuid>66c2109c-2f6d-46d5-9980-a0aa909af060</webElementGuid>
   </webElementProperties>
</WebElementEntity>
