<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_description</name>
   <tag></tag>
   <elementGuidId>f9b22bab-6f11-4b23-8a2f-889b99c1e892</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::textarea[@id=&quot;description&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::textarea[@id=&quot;description&quot;]</value>
      <webElementGuid>c6f4133f-d7bd-4ea6-a9f9-ef8b27f183f8</webElementGuid>
   </webElementProperties>
</WebElementEntity>
