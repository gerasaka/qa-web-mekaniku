<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit</name>
   <tag></tag>
   <elementGuidId>63574807-d58b-42a4-b164-beeac9db2ded</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Simpan&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Simpan&quot;)]</value>
      <webElementGuid>fd4cbb0d-17b3-46f2-ad2a-6336a0b370bb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
