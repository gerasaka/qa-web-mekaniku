<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_open_hour</name>
   <tag></tag>
   <elementGuidId>40ce328c-54f9-46b5-bcff-a186512bcf82</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(), &quot;Jam Buka&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(), &quot;Jam Buka&quot;)]/following-sibling::ngb-timepicker/descendant-or-self::input[1]</value>
      <webElementGuid>2600fff1-01a9-4a3a-a538-b6cef3f1683a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
