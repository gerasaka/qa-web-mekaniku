<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - logout</name>
   <tag></tag>
   <elementGuidId>018c9952-094f-4898-96cc-3b51e9c2ff02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'navbar-menu-item')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class, 'navbar-menu-item')]</value>
      <webElementGuid>3a21d7bb-5518-4ecf-93d2-c474ae4c09d0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
