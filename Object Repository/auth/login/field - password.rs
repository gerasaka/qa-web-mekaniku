<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - password</name>
   <tag></tag>
   <elementGuidId>396c19e7-7fd4-4b85-86d1-5888dc3a1179</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>968124cd-0263-4cc9-abdd-06952ff13403</webElementGuid>
   </webElementProperties>
</WebElementEntity>
