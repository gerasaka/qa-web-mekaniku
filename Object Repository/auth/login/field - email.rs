<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - email</name>
   <tag></tag>
   <elementGuidId>35854339-5161-47e9-bfe9-034b517d215e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='email']</value>
      <webElementGuid>251e7e07-1937-466f-a9c1-845cef3dfa9d</webElementGuid>
   </webElementProperties>
</WebElementEntity>
