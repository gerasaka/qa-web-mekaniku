<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text - validator email</name>
   <tag></tag>
   <elementGuidId>bbc502d0-c511-4d61-b44a-d58c2ef617cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//label[contains(@class,'invalid-feedback')])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//label[contains(@class,'invalid-feedback')])[1]</value>
      <webElementGuid>b9b4f3eb-19b9-4b13-b7b7-d0b2f017223f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
