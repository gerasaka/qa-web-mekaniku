<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>accordion</name>
   <tag></tag>
   <elementGuidId>772a5bf8-cdcb-474d-93de-67b867587985</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class, &quot;accordion-button&quot;)][contains(text(), &quot;${label}&quot;)]</value>
      <webElementGuid>83e27a79-1f57-418c-9f14-bab08cdba754</webElementGuid>
   </webElementProperties>
</WebElementEntity>
