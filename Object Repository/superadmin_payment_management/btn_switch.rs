<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_switch</name>
   <tag></tag>
   <elementGuidId>31f2cdaa-ce83-4abb-8070-9822bae3a073</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[contains(text(), &quot;${payment_name}&quot;)]/ancestor::div[@class=&quot;payment-item&quot;]/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[contains(text(), &quot;${payment_name}&quot;)]/ancestor::div[@class=&quot;payment-item&quot;]/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      <webElementGuid>de07b79f-96c9-4b8f-9416-1319d3d3faa6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
