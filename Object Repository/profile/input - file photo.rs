<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input - file photo</name>
   <tag></tag>
   <elementGuidId>7b510655-b59f-4d18-af86-96990db77506</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type='file']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type='file']</value>
      <webElementGuid>734ab19b-43f0-4a7c-aae4-fb69b9542e0e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
