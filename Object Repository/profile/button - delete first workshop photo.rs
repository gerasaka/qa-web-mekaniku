<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - delete first workshop photo</name>
   <tag></tag>
   <elementGuidId>cfb170d7-6830-4a9c-a075-390261b4fab2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//div[contains(@class,'image-container')]//span[text()='delete'])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//div[contains(@class,'image-container')]//span[text()='delete'])[1]</value>
      <webElementGuid>5d543ccd-688e-4032-b02a-8b5990888344</webElementGuid>
   </webElementProperties>
</WebElementEntity>
