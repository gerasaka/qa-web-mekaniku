<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_change_password</name>
   <tag></tag>
   <elementGuidId>961f8f4b-7f35-4ee2-a450-4eb1d1f7dd10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(text(), &quot;Ganti Password&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(), &quot;Ganti Password&quot;)]</value>
      <webElementGuid>e9e85468-3d05-419e-b8c6-7cd74f02e328</webElementGuid>
   </webElementProperties>
</WebElementEntity>
