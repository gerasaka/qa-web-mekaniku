<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit</name>
   <tag></tag>
   <elementGuidId>3b9d4e19-3c0f-43bc-9e01-6cc83bfc6fb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(text(), &quot;Ubah&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(), &quot;Ubah&quot;)]</value>
      <webElementGuid>874ded14-0736-465e-87f6-a47854255a54</webElementGuid>
   </webElementProperties>
</WebElementEntity>
