<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - edit details</name>
   <tag></tag>
   <elementGuidId>713df558-c67c-414f-b5dd-2a8c265ed09c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//button[span[text()='edit']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//button[span[text()='edit']])[1]</value>
      <webElementGuid>878c1a31-7e02-4324-95d4-cdb185d27edc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
