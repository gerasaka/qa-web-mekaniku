<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - name</name>
   <tag></tag>
   <elementGuidId>3de3a5a9-e338-438e-a170-2fcd53cbe091</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//form//input[@id='name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//form//input[@id='name']</value>
      <webElementGuid>e4307909-f8d7-49b8-bbe0-5c94d4721345</webElementGuid>
   </webElementProperties>
</WebElementEntity>
