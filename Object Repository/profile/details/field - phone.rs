<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - phone</name>
   <tag></tag>
   <elementGuidId>d922b3d9-97f0-4b5d-b6b4-66a31672039d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//form//input[@id='phone']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//form//input[@id='phone']</value>
      <webElementGuid>47cda7df-574f-4658-aedf-aa76b018977b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
