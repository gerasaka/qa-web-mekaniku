<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - address</name>
   <tag></tag>
   <elementGuidId>a77c1308-07f9-4fdf-88df-60570aa2df16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//form//input[@id='address']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//form//input[@id='address']</value>
      <webElementGuid>2a794ef4-e281-4131-9cd8-48bb3f5a3218</webElementGuid>
   </webElementProperties>
</WebElementEntity>
