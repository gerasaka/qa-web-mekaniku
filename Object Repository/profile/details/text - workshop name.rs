<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text - workshop name</name>
   <tag></tag>
   <elementGuidId>e2bd4d1e-130f-45d2-a087-815d90ad195b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-merchant-profile//h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-merchant-profile//h2</value>
      <webElementGuid>4d5d9b75-599e-4044-8d01-7fcd9bea775e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
