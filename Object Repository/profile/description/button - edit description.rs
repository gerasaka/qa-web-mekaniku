<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - edit description</name>
   <tag></tag>
   <elementGuidId>cae7badd-57dd-41b1-b1d3-fc300d5cf229</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//button[span[text()='edit']])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//button[span[text()='edit']])[2]</value>
      <webElementGuid>5236e3f9-d2dd-4fdf-907c-1dc84cf6d4f1</webElementGuid>
   </webElementProperties>
</WebElementEntity>
