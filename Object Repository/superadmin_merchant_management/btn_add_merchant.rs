<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_add_merchant</name>
   <tag></tag>
   <elementGuidId>99403567-03e7-4ecf-b1c0-4c6b616ef675</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(text(), &quot;Tambah Merchant&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(), &quot;Tambah Merchant&quot;)]</value>
      <webElementGuid>67743237-cc86-4d91-8f9f-2f99ac389786</webElementGuid>
   </webElementProperties>
</WebElementEntity>
