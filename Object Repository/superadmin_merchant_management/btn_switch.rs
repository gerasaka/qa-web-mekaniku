<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_switch</name>
   <tag></tag>
   <elementGuidId>bcebe189-7ea7-473d-8347-fa2039dae8ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[contains(text(), &quot;${merchant_id}&quot;)][@class=&quot;mk-link&quot;]/ancestor::td/following-sibling::td/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[contains(text(), &quot;${merchant_id}&quot;)][@class=&quot;mk-link&quot;]/ancestor::td/following-sibling::td/descendant-or-self::input[@type=&quot;checkbox&quot;]</value>
      <webElementGuid>7b703de8-4f92-48ce-8883-41dcd7b566f9</webElementGuid>
   </webElementProperties>
</WebElementEntity>
