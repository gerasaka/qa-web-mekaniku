<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_merchant_id</name>
   <tag></tag>
   <elementGuidId>c2582f1d-4fcb-4d08-8d8e-ae4167f48b73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type=&quot;checkbox&quot;]/ancestor::td/preceding-sibling::td/a[@class=&quot;mk-link&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type=&quot;checkbox&quot;]/ancestor::td/preceding-sibling::td/a[@class=&quot;mk-link&quot;]</value>
      <webElementGuid>e272d29d-6024-4c5b-be7e-8228c7a50622</webElementGuid>
   </webElementProperties>
</WebElementEntity>
