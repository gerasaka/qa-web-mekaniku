<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>table_merchant</name>
   <tag></tag>
   <elementGuidId>b5fc57f8-2dbc-4cb1-8002-89022bf1ca1e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table/descendant-or-self::th[contains(text(),&quot;Merchant ID&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table/descendant-or-self::th[contains(text(),&quot;Merchant ID&quot;)]</value>
      <webElementGuid>2c0573d2-f16b-49b6-a938-def618989efb</webElementGuid>
   </webElementProperties>
</WebElementEntity>
