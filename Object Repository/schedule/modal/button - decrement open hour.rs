<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - decrement open hour</name>
   <tag></tag>
   <elementGuidId>2025808d-c31b-45fa-a257-191b6e21406b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//button[span[text()='Decrement hours']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//button[span[text()='Decrement hours']])[1]</value>
      <webElementGuid>f5a38859-a9d9-4154-861b-1669f23f0ebd</webElementGuid>
   </webElementProperties>
</WebElementEntity>
