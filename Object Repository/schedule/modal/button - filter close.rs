<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - filter close</name>
   <tag></tag>
   <elementGuidId>468ed330-2b2c-43b2-9abc-e977d82ca9c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='Tutup']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Tutup']</value>
      <webElementGuid>a6c3b29e-0495-4fd2-82da-0db01046c8be</webElementGuid>
   </webElementProperties>
</WebElementEntity>
