<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - date</name>
   <tag></tag>
   <elementGuidId>a2b5e6e3-da8a-4b60-8793-f15cc168f0f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='date']</value>
      <webElementGuid>860df240-f40e-4c48-9cd8-0fa228df1b24</webElementGuid>
   </webElementProperties>
</WebElementEntity>
