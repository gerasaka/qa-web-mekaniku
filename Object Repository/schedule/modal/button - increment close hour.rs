<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - increment close hour</name>
   <tag></tag>
   <elementGuidId>7724ac02-c165-4e11-a6f2-c63b8b00ad37</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//button[span[text()='Increment hours']])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//button[span[text()='Increment hours']])[2]</value>
      <webElementGuid>5716f3cc-c433-452c-ac16-e97054fcd539</webElementGuid>
   </webElementProperties>
</WebElementEntity>
