<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - delete first schedule item</name>
   <tag></tag>
   <elementGuidId>8114c6bb-31cb-4056-a132-ca2184567ed1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//td//button[span[text()='delete']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//td//button[span[text()='delete']])[1]</value>
      <webElementGuid>34e070a5-84b2-4180-b063-d9be7032c338</webElementGuid>
   </webElementProperties>
</WebElementEntity>
