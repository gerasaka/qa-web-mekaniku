<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card - empty schedule</name>
   <tag></tag>
   <elementGuidId>3e683c83-ce51-4be1-ba3a-526cbe82e2f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-empty-placeholder</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-empty-placeholder</value>
      <webElementGuid>1c94e106-0f60-4197-b743-44c5b3f52b29</webElementGuid>
   </webElementProperties>
</WebElementEntity>
