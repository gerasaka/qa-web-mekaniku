<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_header</name>
   <tag></tag>
   <elementGuidId>4859535b-8018-4c4b-a471-364885b22898</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::h2[contains(text(), &quot;Sukses Menambah Merchant&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::h2[contains(text(), &quot;Sukses Menambah Merchant&quot;)]</value>
      <webElementGuid>12ad7419-9d22-4639-8658-04fa942a3903</webElementGuid>
   </webElementProperties>
</WebElementEntity>
