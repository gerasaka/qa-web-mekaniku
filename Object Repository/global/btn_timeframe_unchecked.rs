<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_timeframe_unchecked</name>
   <tag></tag>
   <elementGuidId>c5eee843-c647-454d-982c-3edee1c79267</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[@for=&quot;${id}&quot;][contains(@class, &quot;btn-text&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@for=&quot;${id}&quot;][contains(@class, &quot;btn-text&quot;)]</value>
      <webElementGuid>4f5e0a10-a0d6-48e0-9d60-160e49c1b7a6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
