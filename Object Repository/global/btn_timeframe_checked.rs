<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_timeframe_checked</name>
   <tag></tag>
   <elementGuidId>1014fb3e-c8cc-4433-b621-4196595052a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[@for=&quot;${id}&quot;][contains(@class, &quot;btn-primary&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[@for=&quot;${id}&quot;][contains(@class, &quot;btn-primary&quot;)]</value>
      <webElementGuid>036864a6-6388-4410-8628-09ecad58c2ac</webElementGuid>
   </webElementProperties>
</WebElementEntity>
