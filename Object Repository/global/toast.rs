<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toast</name>
   <tag></tag>
   <elementGuidId>3c1c8512-c478-4458-8c6f-4269ef87cf87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;toast-body&quot;]/descendant-or-self::span[contains(text(), &quot;Bengkel&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;toast-body&quot;]/descendant-or-self::span[contains(text(), &quot;${value}&quot;)]</value>
      <webElementGuid>cc75ba6d-2a82-4005-9b78-a37383ab9a88</webElementGuid>
   </webElementProperties>
</WebElementEntity>
