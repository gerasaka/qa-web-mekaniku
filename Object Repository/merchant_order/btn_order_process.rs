<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_process</name>
   <tag></tag>
   <elementGuidId>50f7d30f-255d-400a-a50c-150800de7eb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Proses&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Proses&quot;)]</value>
      <webElementGuid>3615669d-4aa7-43f8-82d3-ead973120942</webElementGuid>
   </webElementProperties>
</WebElementEntity>
