<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_done</name>
   <tag></tag>
   <elementGuidId>852d1996-00ad-4f9b-91d5-fe8452c11776</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Selesai&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Selesai&quot;)]</value>
      <webElementGuid>b1509dac-67d0-4241-8728-ccc19b7edc47</webElementGuid>
   </webElementProperties>
</WebElementEntity>
