<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_reject</name>
   <tag></tag>
   <elementGuidId>0a706242-84af-4051-9f11-e122ab1fc64e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Tolak&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Tolak&quot;)]</value>
      <webElementGuid>2f93fbcb-62e9-4f14-8e8a-b726e0f69dc3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
