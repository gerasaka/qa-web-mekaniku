<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_invoice</name>
   <tag></tag>
   <elementGuidId>12a061dd-e196-4b4d-8d4a-1063c5314819</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Lihat Invoice&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Lihat Invoice&quot;)]</value>
      <webElementGuid>1bfe311b-7f22-4db5-9b79-785f1b228a2f</webElementGuid>
   </webElementProperties>
</WebElementEntity>
