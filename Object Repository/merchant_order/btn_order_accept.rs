<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_accept</name>
   <tag></tag>
   <elementGuidId>874f24ed-8f29-43fc-9fb9-de27e62bfc47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Terima&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Terima&quot;)]</value>
      <webElementGuid>0a850661-f0fc-4da1-92e1-b84eb5599ec5</webElementGuid>
   </webElementProperties>
</WebElementEntity>
