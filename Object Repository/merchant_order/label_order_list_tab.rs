<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_order_list_tab</name>
   <tag></tag>
   <elementGuidId>4d7559f2-25f9-4bf3-90b3-618d2b1559a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(@class, &quot;filter-label&quot;)][contains(text(), &quot;${label}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(@class, &quot;filter-label&quot;)][contains(text(), &quot;${label}&quot;)]</value>
      <webElementGuid>80d9c671-8940-4c36-bed7-58a4356a9667</webElementGuid>
   </webElementProperties>
</WebElementEntity>
