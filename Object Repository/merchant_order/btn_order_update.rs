<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_order_update</name>
   <tag></tag>
   <elementGuidId>c713fdaf-1df2-457e-8734-5a5e167022a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Perbarui&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//app-order-item/descendant-or-self::button[contains(text(), &quot;Perbarui&quot;)]</value>
      <webElementGuid>15cc4f1d-27a3-4ef3-9b7c-9c73a7bf9cd7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
