<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_order_list_tab_active</name>
   <tag></tag>
   <elementGuidId>15a1b7ea-7d02-42f7-bb6a-d015ca1dada3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(@class, &quot;filter-label&quot;)][contains(@class, &quot;btn-primary&quot;)][contains(text(), &quot;${label}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(@class, &quot;filter-label&quot;)][contains(@class, &quot;btn-primary&quot;)][contains(text(), &quot;${label}&quot;)]</value>
      <webElementGuid>c21d7f0e-119e-4aa7-a00b-449628fb44ea</webElementGuid>
   </webElementProperties>
</WebElementEntity>
