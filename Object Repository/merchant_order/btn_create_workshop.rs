<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_create_workshop</name>
   <tag></tag>
   <elementGuidId>b7aed10d-a456-46a7-b174-2f5e1a245f48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(text(), &quot;Buat Bengkel&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(text(), &quot;Buat Bengkel&quot;)]</value>
      <webElementGuid>9b09550c-840e-4259-b7b5-8b15083235a4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
