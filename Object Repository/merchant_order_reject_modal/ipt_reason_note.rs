<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_reason_note</name>
   <tag></tag>
   <elementGuidId>68e149cf-a015-4e85-9047-702d0e2f940b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@placeholder=&quot;Masukkan alasan menolak disini&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//textarea[@placeholder=&quot;Masukkan alasan menolak disini&quot;]</value>
      <webElementGuid>548d501b-1d94-4cdb-a8ac-b0d726f5cbfd</webElementGuid>
   </webElementProperties>
</WebElementEntity>
