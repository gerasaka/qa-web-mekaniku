<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit</name>
   <tag></tag>
   <elementGuidId>4cc7cd4f-0f29-4c8e-9212-aa69ee1cf1c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Tolak&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Tolak&quot;)]</value>
      <webElementGuid>797c06d3-411c-4068-a22c-daaf0eb8354c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
