<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit</name>
   <tag></tag>
   <elementGuidId>95ea4aec-b104-47a5-bbde-e08503d49804</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Tambah&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Tambah&quot;)]</value>
      <webElementGuid>6f4901ac-56ee-45b9-b007-97675f200312</webElementGuid>
   </webElementProperties>
</WebElementEntity>
