<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_ipt_error</name>
   <tag></tag>
   <elementGuidId>a59c015e-5fb6-4577-a50e-7d30a382aa02</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::label[contains(@class, &quot;invalid-feedback&quot;)][contains(text(), &quot;${value}&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::label[contains(@class, &quot;invalid-feedback&quot;)][contains(text(), &quot;${value}&quot;)]</value>
      <webElementGuid>254db5a4-2619-4d3c-96be-cd35a71f7e9c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
