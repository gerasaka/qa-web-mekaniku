<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_header</name>
   <tag></tag>
   <elementGuidId>32ee5b46-8bde-43ca-86a1-bbce4ee79e2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::h2[contains(text(), &quot;Tambah Merchant&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::h2[contains(text(), &quot;Tambah Merchant&quot;)]</value>
      <webElementGuid>e662838a-2d5f-4557-8483-1e5179607754</webElementGuid>
   </webElementProperties>
</WebElementEntity>
