<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_submit</name>
   <tag></tag>
   <elementGuidId>a369944c-1cfa-4a00-b0c2-8c50fc995a12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Simpan&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class=&quot;modal-content&quot;]/descendant-or-self::button[contains(text(), &quot;Simpan&quot;)]</value>
      <webElementGuid>0715ec01-9aea-40d2-9fac-82dd10ca8319</webElementGuid>
   </webElementProperties>
</WebElementEntity>
