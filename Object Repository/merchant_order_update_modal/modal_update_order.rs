<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>modal_update_order</name>
   <tag></tag>
   <elementGuidId>21437257-b4fd-4165-8ec1-5bf3900d5c32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[contains(text(), &quot;ID&quot;)]/ancestor-or-self::div[@class=&quot;modal-content&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[contains(text(), &quot;ID&quot;)]/ancestor-or-self::div[@class=&quot;modal-content&quot;]</value>
      <webElementGuid>665c3a97-9704-413c-acc3-3fc4fe05e7f6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
