<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ipt_workshop_note</name>
   <tag></tag>
   <elementGuidId>0663be26-4be0-4e6f-83ed-1a9f30b4054f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@placeholder=&quot;Masukkan catatan disini&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//textarea[@placeholder=&quot;Masukkan catatan disini&quot;]</value>
      <webElementGuid>9ba4a044-6f28-4649-925e-97615588de1e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
