<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - profile menu</name>
   <tag></tag>
   <elementGuidId>b4a77f65-4abf-4bc8-8e18-99a91bc61e9b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[contains(@class, 'navbar')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[contains(@class, 'navbar')]</value>
      <webElementGuid>cc280f93-24d1-4ed4-84fc-28e181805052</webElementGuid>
   </webElementProperties>
</WebElementEntity>
