<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toast - danger</name>
   <tag></tag>
   <elementGuidId>61446d2e-5ea6-4db1-9e6d-49da708b4bad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>contains(@class, 'danger')</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//ngb-toast[contains(@class, 'danger')] </value>
      <webElementGuid>09f18429-0e20-4667-8b29-1d78ea65c27b</webElementGuid>
   </webElementProperties>
</WebElementEntity>
