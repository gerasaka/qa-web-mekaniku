<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toast - success</name>
   <tag></tag>
   <elementGuidId>d9efc2ab-8412-422a-8bb8-9ac6e31348d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//ngb-toast[contains(@class,'success')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//ngb-toast[contains(@class,'success')]</value>
      <webElementGuid>69855617-e4ce-44b4-b2f1-465ffec803f6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
