<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>title - modal</name>
   <tag></tag>
   <elementGuidId>3f77c1b1-1390-4743-abba-60684b74d76c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[@id='modal-basic-title']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[@id='modal-basic-title']</value>
      <webElementGuid>cc134f41-35bc-44dd-95ad-60d4015c7425</webElementGuid>
   </webElementProperties>
</WebElementEntity>
