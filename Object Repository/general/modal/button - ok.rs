<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - ok</name>
   <tag></tag>
   <elementGuidId>980dffc8-0cda-4313-bf7b-ce349dc96793</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//parent::div[@class='modal-footer']/button)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//parent::div[@class='modal-footer']/button)[2]</value>
      <webElementGuid>37853fec-5ef7-424f-9cb9-4efa3e616e89</webElementGuid>
   </webElementProperties>
</WebElementEntity>
