<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>title - delete modal</name>
   <tag></tag>
   <elementGuidId>86e7e82a-0057-4dbd-b336-6f311462a70b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h2[contains(text(), 'Hapus')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h2[contains(text(), 'Hapus')]</value>
      <webElementGuid>d397148a-c359-48d8-94b9-1dee9c778728</webElementGuid>
   </webElementProperties>
</WebElementEntity>
