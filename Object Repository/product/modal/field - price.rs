<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>field - price</name>
   <tag></tag>
   <elementGuidId>6a087dd4-5588-4d6f-8238-a65905c1975f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id='price']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@id='price']</value>
      <webElementGuid>8ca331af-cb7a-42e2-87de-b8218f07c8fc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
