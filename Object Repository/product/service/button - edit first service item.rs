<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - edit first service item</name>
   <tag></tag>
   <elementGuidId>fad0995b-22a5-4e2a-8c72-f3c115038ba2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>((//table)[1]//button[span[text()='edit']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>((//table)[1]//button[span[text()='edit']])[1]</value>
      <webElementGuid>cec28dee-4141-4fc4-8514-2f485569655a</webElementGuid>
   </webElementProperties>
</WebElementEntity>
