<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - delete first good item</name>
   <tag></tag>
   <elementGuidId>5ce776c3-f3b3-4610-820f-9f11c6757ebb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>((//table)[1]//button[span[text()='delete']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>((//table)[1]//button[span[text()='delete']])[1]</value>
      <webElementGuid>ed5b7e19-6d5a-495a-9054-4bc728ca8e53</webElementGuid>
   </webElementProperties>
</WebElementEntity>
