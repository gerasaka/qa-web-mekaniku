<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>check - second good item</name>
   <tag></tag>
   <elementGuidId>e57d869c-b1d7-440b-9cb4-ebbebb4dcdbf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//input[@type='checkbox'])[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[3]</value>
      <webElementGuid>875485d3-f529-4be6-bbb4-e0e880424397</webElementGuid>
   </webElementProperties>
</WebElementEntity>
