<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - delete first good item</name>
   <tag></tag>
   <elementGuidId>d7616638-869c-407b-a51e-299730daed45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>((//table)[2]//button[span[text()='delete']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>((//table)[2]//button[span[text()='delete']])[1]</value>
      <webElementGuid>ea2afb2e-3cc5-469d-92ef-ef387a073dbe</webElementGuid>
   </webElementProperties>
</WebElementEntity>
