<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - delete multiple goods</name>
   <tag></tag>
   <elementGuidId>e2d3b72d-0f60-4fb0-aca3-83f1d4d6d168</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[text()=' Hapus Barang ']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[text()=' Hapus Barang ']</value>
      <webElementGuid>5571c29b-b17c-4de8-bb13-36db9c2b04c2</webElementGuid>
   </webElementProperties>
</WebElementEntity>
