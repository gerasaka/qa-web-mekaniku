<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button - edit first item good</name>
   <tag></tag>
   <elementGuidId>e1660eca-56ed-4c47-bf78-7abfd5686f9a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>((//table)[2]//button[span[text()='edit']])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>((//table)[2]//button[span[text()='edit']])[1]</value>
      <webElementGuid>3b2f823d-7368-4648-9b57-ea506f32ed72</webElementGuid>
   </webElementProperties>
</WebElementEntity>
