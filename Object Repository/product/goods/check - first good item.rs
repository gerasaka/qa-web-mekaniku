<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>check - first good item</name>
   <tag></tag>
   <elementGuidId>725d7b1f-ab45-4115-b6f2-58d7eee05d20</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//input[@type='checkbox'])[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//input[@type='checkbox'])[2]</value>
      <webElementGuid>a879ba67-6304-4358-9061-f11fd26e320e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
