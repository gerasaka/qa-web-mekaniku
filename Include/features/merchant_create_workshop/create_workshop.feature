#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_create_workshop
Feature: Create Workshop

  Background: Success to Login as Merchant and not Created Workshop Yet 
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login
    
  @merchant_create_workshop_success
  Scenario: Success to Create Workshop
    When I click create workshop
    Then I verify create workshop modal
    When I type workshop name "Bengkel" in create workshop
    And I type address "Jl. Sudirman No. 6, Jakarta" in create workshop
    And I type description "Deskripsi singkat" in create workshop
    And I type open time "08" and "00" in create workshop
    And I type close time "17" and "00" in create workshop
    And I click car as workshop type
    And I click btn submit in create workshop
    Then I verify toast "Bengkel anda sudah aktif"
    