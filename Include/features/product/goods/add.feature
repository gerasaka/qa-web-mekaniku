@addGood
Feature: Add new good(s) to inventory
  As a merchant, I want to add new good
  So that I can access it when processing order
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I add new good with valid request
  	Given I open product page
    When I click open add "GOODS" modal button
    Then I verify product modal displayed
    When I input product "Oli Mesin", and "50000"
    And I click add product button
    Then I verify success toast displayed
    
  @cancel
  Scenario:  I add cancel add new good
  	Given I open product page
    When I click open add "GOODS" modal button
    Then I verify product modal displayed
    When I input product "Oli Mesin", and "50000"
    And I click cancel button
    Then I verify product modal is not displayed
    