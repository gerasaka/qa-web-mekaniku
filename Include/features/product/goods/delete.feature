@deleteGood
Feature: Delete good item
  As a merchant, I want to delete good item 
  So that I can remove good from goods list
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I delete good item
    Given I open product page
    When I click first "GOODS" item delete button
    Then I verify delete modal displayed
    When I click confirm delete product button
    Then I verify success toast displayed
    
  @cancel
  Scenario:  I cancel delete good item
  	Given I open product page
    When I click first "GOODS" item delete button
    Then I verify delete modal displayed
    When I click cancel button
    Then I verify product modal is not displayed
    