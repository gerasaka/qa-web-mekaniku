@editGood
Feature: Edit good
  As a merchant, I want to edit good data
  So that I can change the data as needed
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I edit good with valid request
    Given I open product page
    When I click first edit "GOODS" button
    Then I verify product modal displayed
    When I input product "Oli Mesin Motor", and "45000"
    And I click save product button
    Then I verify success toast displayed
    
  @cancel
  Scenario:  I cancel edit good
    Given I open product page
    When I click first edit "GOODS" button
    Then I verify product modal displayed
    When I input product "Oli Mesin Motor", and "45000"
    And I click cancel button
    Then I verify product modal is not displayed
  