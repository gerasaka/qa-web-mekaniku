@deleteMultipleGood
Feature: Delete multiple good
  As a merchant, I want to delete multiple data 
  So that I can remove multiple good with one delete action
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I delete multiple good items
    Given I open product page
    When I select the first and second of goods items
    And I click delete multiple good button
    Then I verify delete modal displayed
    When I click confirm delete product button
    Then I verify success toast displayed
    
  @cancel
  Scenario:  I cancel delete multiple good items
    Given I open product page
    When I select the first and second of goods items
    And I click delete multiple good button
    Then I verify delete modal displayed
    When I click cancel button
    Then I verify product modal is not displayed
    