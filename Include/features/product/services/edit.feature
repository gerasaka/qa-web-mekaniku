@editService
Feature: Edit service
  As a merchant, I want to edit service data
  So that I can change the data as needed

	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I edit service with valid request
    Given I open product page
    When I click first edit "SERVICE" button
    Then I verify product modal displayed
    When I input product "Servis Berat", and "30000"
    And I click save product button
    Then I verify success toast displayed
  
  @cancel
  Scenario:  I cancel edit service
    Given I open product page
    When I click first edit "SERVICE" button
    Then I verify product modal displayed
    When I input product "Servis Berat", and "30000"
    And I click cancel button
    Then I verify product modal is not displayed
  
    
    