@deleteService
Feature: Delete service from sevice list
  As a merchant, I want to delete service item 
  So that user customer can't order for the service

	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I delete service item
    Given I open product page
    When I click first "SERVICE" item delete button
    Then I verify delete modal displayed
    When I click confirm delete product button
    Then I verify success toast displayed
  
  @cancel
  Scenario:  I cancel delete service item
    Given I open product page
    When I click first "SERVICE" item delete button
    Then I verify delete modal displayed
    When I click cancel button
    Then I verify product modal is not displayed
    