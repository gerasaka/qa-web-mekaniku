@addService
Feature: Add new service(s) to sevice list
  As a merchant, I want to add new service
  So that customer can see list of service on the mobile app
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I add new service with valid request
  	Given I open product page
    When I click open add "SERVICE" modal button
    Then I verify product modal displayed
    When I input product "Servis Ringan", and "20000"
    And I click add product button
    Then I verify success toast displayed
  
  @cancel
  Scenario:  I cancel add new service
  	Given I open product page
    When I click open add "SERVICE" modal button
    Then I verify product modal displayed
    When I input product "Servis Ringan", and "20000"
    And I click cancel button
    Then I verify product modal is not displayed
    
  
    