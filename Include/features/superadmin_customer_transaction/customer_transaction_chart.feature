#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@customer_transaction_chart
Feature: Customer Transaction Chart

  Background: Success to Login as Superadmin and open Customer Transaction
  	Given I open the web
    When I type email "superadmin@email.example" in login
    And I type password "secretpassword" in login
    And I click submit login
    And I click customer id
    Then I verify superadmin customer transaction page
    
  @customer_transaction_chart_7_day
  Scenario: Success to Show Default 7 Day Timeframe Chart Transaction
    Then I verify customer transaction 7 day timeframe chart 
    
  @customer_transaction_chart_30_day
  Scenario: Success to Show Default 30 Day Timeframe Chart Transaction
  	When I click 30 day timeframe button 
    Then I verify customer transaction 30 day timeframe chart 