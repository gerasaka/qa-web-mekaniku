#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@customer_management_update_status
Feature: Customer Management Update Status

  Background: Success to Login as Superadmin and Customer List is Shown
    Given I open the web
    When I type email "superadmin@email.example" in login
    And I type password "secretpassword" in login
    And I click submit login
    Then I verify customer list table
    
  @customer_management_update_status_success
  Scenario Outline: Success to Change Customer Status
    When I click customer item <value_before> switch 
    Then I verify customer item status is changed to <value_after>
    
    Examples: 
      | value_before | value_after |
      | true | false |
      | false | true |