#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_order_list
Feature: Merchant Order List

	Background: Success to Login as Merchant and Have Created Workshop
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login
    Then I verify merchant order management page

  @merchant_order_list_booked
  Scenario: Success to Fetch Booked List
    Then I verify "Menunggu Konfirmasi" order list tab active
    When I click accept order btn in order list
    Then I verify toast "Status order berhasil diubah"
    
  @merchant_order_list_accepted
  Scenario: Success to Fetch Accepted List
  	When I click "Menunggu Datang" order list tab
    Then I verify "Menunggu Datang" order list tab active
		When I click process order btn in order list
		And I click submit in update order modal
    Then I verify toast "Status order berhasil diubah"
    
  @merchant_order_list_in_process
  Scenario: Success to Fetch In Process List
  	When I click "Diproses" order list tab
    Then I verify "Diproses" order list tab active
    When I click done order btn in order list
    Then I verify toast "Status order berhasil diubah"
    
  @merchant_order_list_processed
  Scenario: Success to Fetch Processed List
  	When I click "Menunggu Bayar" order list tab
    Then I verify "Menunggu Bayar" order list tab active
    
  @merchant_order_list_done
  Scenario: Success to Fetch Done List
  	When I click "Selesai" order list tab
    Then I verify "Selesai" order list tab active
    When I click invoice btn in order list
    Then I verify invoice modal
    
  @merchant_order_list_canceled
  Scenario: Success to Fetch Canceled List
  	When I click "Batal" order list tab
    Then I verify "Batal" order list tab active
    
  @merchant_order_list_rejected
  Scenario: Success to Fetch Rejected List
  	When I click "Ditolak" order list tab
    Then I verify "Ditolak" order list tab active