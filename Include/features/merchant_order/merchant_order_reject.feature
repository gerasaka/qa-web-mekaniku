#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_order_reject
Feature: Reject Merchant Order 

	Background: Success to Login as Merchant and Open Order Management Page 
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login

  @merchant_order_reject_success
  Scenario: Success to Reject Order
  	When I click reject order btn in order list
    And I type reason note "Bengkel penuh"
    And I click submit in reject order modal
    Then I verify toast "Penolakan pesanan berhasil dikirim"