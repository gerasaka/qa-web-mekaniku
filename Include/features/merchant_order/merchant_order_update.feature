#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_order_update
Feature: Update Merchant Order 

	Background: Success to Login as Merchant and Open Order Management Page in In Process Tab 
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login
    And I click "Diproses" order list tab
    Then I verify "Diproses" order list tab active

  @merchant_order_update_success
  Scenario: Success to Update Order
  	When I click update order btn in order list
    And I type workshop note "Ganti catatan"
    And I click submit in update order modal
    Then I verify toast "Order berhasil diperbarui"