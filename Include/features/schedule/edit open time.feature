@editOpenTime
Feature: Edit open time
  As a merchant, I want to edit my workshop open time
  So that customer can't make an order when close time
	
	Background: User is Logged In
	Given I open app
	And I login to the app
		
  @success
  Scenario:  I add half day schedule with valid request
  	Given I open schedule page
    When I click open open time modal button
    Then I verify schedule modal displayed
    When I input open hour and close hour
    And I click add schedule button
    Then I verify success toast displayed
   