@deleteSchedule
Feature: Delete one schedule
  As a merchant, I want to delete one schedule
  So that remove a schedule from schedule table
	
	Background: User is Logged In
	Given I open app
	And I login to the app
		
  @success
  Scenario:  I delete first schedule
  	Given I open schedule page
    When I click first schedule delete button
    Then I verify schedule modal displayed 
    When I click confirm delete schedule
    Then I verify success toast displayed
   