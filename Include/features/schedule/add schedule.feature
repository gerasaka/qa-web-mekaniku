@addSchedule
Feature: Add schedule
  As a merchant, I want to add schedule when my workshop need to close or open half day
  So that customer can't make an order when close time
	
	Background: User is Logged In
	Given I open app
	And I login to the app
		
  @successAddHalfDay
  Scenario:  I add half day schedule with valid request
  	Given I open schedule page
    When I click open add schedule button
    Then I verify schedule modal displayed
    When I choose for date
    And I click half day filter
    And I input open hour and close hour
    And I click add schedule button
    #Then I verify success toast displayed

  @successAddClose
  Scenario:  I add close schedule with valid request
  	Given I open schedule page
    When I click open add schedule button
    Then I verify schedule modal displayed
    When I choose for date
    And I click close filter
    And I click add schedule button
    #Then I verify success toast displayed
   