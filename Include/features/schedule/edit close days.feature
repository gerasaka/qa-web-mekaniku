@editCloseDays
Feature: Edit close days
  As a merchant, I want to edit my workshop close days
  So that customer can't make an order when my workshop is close
	
	Background: User is Logged In
	Given I open app
	And I login to the app
		
  @success
  Scenario:  I edit close days
  	Given I open schedule page
    When I click open close days modal button
    Then I verify schedule modal displayed
    And I select sunday
    And I click add schedule button
    Then I verify success toast displayed
   