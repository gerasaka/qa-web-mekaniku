@workshopPhoto
Feature: Upload and delete photo
  As a merchant, I want to upload and delete workshop photo
  So that customer can see image of my workshop
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	
  #@successUploadPhoto
  #Scenario:  I upload workshop photo with size 1Mb or less
  #	Given I open profile page
    #When I select workshop photo
    #Then I verify success toast displayed
  #
  #@failedUploadPhoto
  #Scenario:  I upload workshop photo with size more than 1Mb
  #	Given I open profile page
    #When I select big workshops photo
    #Then I verify danger toast displayed
  
  @deletePhoto
  Scenario:  I delete workshop photo
  	Given I open profile page
    When I click delete first workshop photo
    Then I verify delete modal displayed
    And I click delete photo button
    Then I verify success toast displayed