@editWorkshopDescription
Feature: Edit profile description
  As a merchant, I want to edit workshop description
  So that customer can see my newest info about my workshop

	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario Outline:  I edit workshop description with valid request
    Given I open profile page 
    When I click edit description button
    And I input new <description>
    And I click save description button
    Then I verify success toast displayed
    
  Examples:
  | description                                          |
  | Bengkel mobil dan motor, melayani servis dan lainnya |
    