@editProfileDetails
Feature: Edit profile details
  As a merchant, I want to edit profile details data
  So that I can change the data as needed
	
	Background: User is Logged In
	Given I open app
	And I login to the app
	And I input avatar image
		
  @success
  Scenario Outline:  I edit profile details with valid request
    When I input workshop <name>, <phone> and <address>
    And I click save detail button
    Then I verify workshop <name> displayed
    
   	Examples: 
   	| name          | phone    | address         |
   	| Bengkel Mulia | 08123123 | Jakarta selatan |
    
    