#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@change_password
Feature: Change Password
  
  Background: Success to Login as Merchant
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login

  @change_password_success
  Scenario: Success to Change Password
    Given I open profile page 
    When I click change password btn in profile
    And I type old password "Ee1<]PTn" in change password modal
    And I type new password "Ee1<]PTn" in change password modal 
    And I type confirm password "Ee1<]PTn" in change password modal
    And I click submit btn in change password modal
    Then I verify toast "Berhasil mengubah password"