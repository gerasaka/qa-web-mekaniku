@selectAvatar
Feature: Select avatar image
  As a merchant, I want to select avatar
  So that I can upload and user can see it

	Background: User is Logged In
	Given I open app
	And I login to the app
	
  @success
  Scenario:  I select avatar image with size 1Mb or less
    Given I open profile page
    When I click edit details button
    Then I verify profile modal displayed
    And I select avatar image
    #Then I verify image choosen displayed
    
    