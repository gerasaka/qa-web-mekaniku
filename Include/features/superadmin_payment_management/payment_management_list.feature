#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@payment_management_list
Feature: Payment Management

  Background: Success to Login as Superadmin and Open Payment Management Page
  	Given I open the web
    When I type email "superadmin@email.example" in login
    And I type password "secretpassword" in login
    And I click submit login
    And I click payment nav item

	@payment_management_list_success
  Scenario: Success to Show Virtual Account and Emoney
    Then I verify virtual account and emoney