#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_transaction_detail
Feature: Merchant Transaction Detail

  Background: Success to Login as Merchant and open Merchant Transaction
  	Given I open the web
    When I type email "workshop20@gmail.com" in login
    And I type password "Ee1<]PTn" in login
    And I click submit login
    And I click transaction nav item
    Then I verify merchant transaction page
    
  @merchant_transaction_detail_success
  Scenario: Success to open Merchant Transaction Detail Modal
  	When I click transaction id
    Then I verify merchant transaction detail