#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@merchant_management_add_merchant
Feature: Add Merchant

  Background: Success to Login as Superadmin and Open Merchant Management Page
  	Given I open the web
    When I type email "superadmin@email.example" in login
    And I type password "secretpassword" in login
    And I click submit login
    And I verify superadmin customer management page
    And I click merchant nav item
    Then I verify superadmin merchant management page
    
  @merchant_management_add_merchant_success
  Scenario: Success to Add Merchant
    When I click add merchant
    Then I verify add merchant modal
    And I type owner name "Owner" in add merchant
    And I type email "owner@gmail.com" in add merchant
    And I type phone "08123123123" in add merchant
    And I click submit add merchant
    Then I verify success add merchant modal
    