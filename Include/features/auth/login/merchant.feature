@loginMerchant
Feature: Login merchant
  As a merchant, I want to login to the app
  So that I can access merchant dasboard

  @success
  Scenario:  I login to the app with valid credentials
  	Given I open app
    When I input "workshop20@gmail.com", and "Ee1<]PTn"
    And I click login button
    Then I verify merchant 'Daftar Order' page displayed
    