@loginMerchant
Feature: Login superadmin
  As superadmin, I want to login to the app
  So that I can access superadmin dasboard

  @success
  Scenario:  I login to the app with valid credentials
    Given I open app
    When I input "superadmin@email.example", and "secretpassword"
    And I click login button
    Then I verify superadmin 'Kustomer' page displayed
    