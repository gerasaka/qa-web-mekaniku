@failedLogin
Feature: Login

  @failedWithWrongPassword
  Scenario:  I login to the app with invalid credentials
  	Given I open app
    When I input "workshop20@gmail.com", and "myPass123"
    And I click login button
    Then I verify wrong credentials toast displayed
    
  @failedWithUnregisteredEmail
  Scenario:  I login to the app with unregistered email
  	Given I open app
    When I input "someone@user.co", and "userPass123"
    And I click login button
    Then I verify wrong credentials toast displayed
   
  @failedWithIncompleteCredentials
  Scenario Outline: I login to the app with incomplete credentials
  	Given I open app
    When I input "<email>", and "<password>"
    And I click login button
    Then I verify <validator> validator <message> displayed
    
  	Examples:
  	| email         | password | validator    | message                  |
  	|               | pass123  | email        | Email harus diisi        |
  	| email@mail.co |          | password     | Password harus diisi     |
  	| email.co      | pass123  | email        | Format email tidak valid |
    
    