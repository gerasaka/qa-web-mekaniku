@logout
Feature: Logout user
  As a user, I want to logout from the app
  So that other people can't access my account when I logout

	Background:
		Given I open app

  @success
  Scenario:  I logout from the app
  	Given I login to the app
    When I open profile menu 
    And I click logout button
    Then I verify login page displayed
    