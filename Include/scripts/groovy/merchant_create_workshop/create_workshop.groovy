package merchant_create_workshop
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class create_workshop {
	@When("I click create workshop")
	def I_click_create_workshop() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_create_workshop'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type workshop name {string} in create workshop")
	def I_type_workshop_name_in_create_workshop(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_type_workshop_name_in_create_workshop'), [('fullname'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type address {string} in create workshop")
	def I_type_address_in_create_workshop(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_type_address_in_create_workshop'), [('address'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type description {string} in create workshop")
	def I_type_description_in_create_workshop(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_type_description_in_create_workshop'), [('description'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type open time {string} and {string} in create workshop")
	def I_type_open_time_in_create_workshop(String open_hour, String open_minute) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_type_open_time_in_create_workshop'), [('open_hour'):open_hour, ('open_minute'):open_minute], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type close time {string} and {string} in create workshop")
	def I_type_close_time_in_create_workshop(String close_hour, String close_minute) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_type_close_time_in_create_workshop'), [('close_hour'):close_hour, ('close_minute'):close_minute], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click car as workshop type")
	def I_click_car_as_workshop_type() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_click_car_as_workshop_type'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click btn submit in create workshop")
	def I_click_btn_submit_in_create_workshop() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_click_btn_submit_in_create_workshop'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify create workshop modal")
	def I_verify_create_workshop_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_create_workshop/i_verify_create_workshop_modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}