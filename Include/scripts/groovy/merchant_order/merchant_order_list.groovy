package merchant_order
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class merchant_order_list {
	@When("I click {string} order list tab")
	def I_click_order_list_tab(String label) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_order_list_tab'), [('label'):label], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click reject order btn in order list")
	def I_click_reject_order_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_reject_order_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click accept order btn in order list")
	def I_click_accept_order_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_accept_order_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click process order btn in order list")
	def I_click_process_order_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_process_order_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click update order btn in order list")
	def I_click_update_order_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_update_order_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click done order btn in order list")
	def I_click_done_order_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_done_order_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click invoice btn in order list")
	def I_click_invoice_btn_in_order_list() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_click_invoice_btn_in_order_list'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify merchant order management page")
	def I_verify_merchant_order_management_page() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_verify_merchant_order_management_page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify {string} order list tab active")
	def I_verify_order_list_tab_active(String label) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/merchant_order/i_verify_order_list_tab_active'), [('label'):label], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify invoice modal")
	def I_verify_invoice_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/global/i_verify_order_modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}