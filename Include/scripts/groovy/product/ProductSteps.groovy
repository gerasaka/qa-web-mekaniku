package product
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class ProductSteps {
	@Given("I open product page")
	def I_open_product_page() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/open product page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input product {string}, and {string}")
	def I_input_product_name_and_price(String name, String price) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/input product name and price'), ['name' : name, 'price' : price], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I click add product button")
	def I_click_add_product_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/general/click ok in modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click confirm delete product button")
	def I_click_delete_product_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/delete product/click confirm delete button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify product modal displayed")
	def I_verify_product_modal_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/verify product modal displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify product modal is not displayed")
	def I_verify_product_modal_is_not_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/verify product modal is not displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}