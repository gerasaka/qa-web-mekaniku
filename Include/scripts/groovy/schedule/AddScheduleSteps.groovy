package schedule
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddScheduleSteps {
	@When("I click open add schedule button")
	def I_click_open_add_schedule_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/schedule/add schedule/open add schedule modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I choose for date")
	def I_choose_for_date() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/schedule/add schedule/choose for date'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I click half day filter")
	def I_click_half_day_filter() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/schedule/add schedule/click half day filter'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I click close filter")
	def I_click_close_filter() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/schedule/add schedule/click close filter'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I click add schedule button")
	def I_click_add_schedule_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/schedule/add schedule/click add schedule button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}