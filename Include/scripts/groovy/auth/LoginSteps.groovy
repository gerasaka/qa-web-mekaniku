package auth
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LoginSteps {
	@Given("I login to the app")
	def I_login() {
		I_input_merchant_email_and_password("workshop20@gmail.com", "Ee1<]PTn")
		I_click_login_button()
		I_verify_order_list_page_displayed()
	}

	@When("I input {string}, and {string}")
	def I_input_merchant_email_and_password(String email, String pass) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/input user credentials'), ['email' : email, 'pass' : pass], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I click login button")
	def I_click_login_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/click login button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify merchant 'Daftar Order' page displayed")
	def I_verify_order_list_page_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/verify order list page displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify superadmin 'Kustomer' page displayed")
	def I_verify_customer_page_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/verify customer page displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify wrong credentials toast displayed")
	def I_verify_login_page_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/verify toast failed login displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify (.*) validator (.*) displayed")
	def I_verify_validator_message_displayed(String validator, String message) {
		if (validator == 'email') {
			WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/verify validator email message displayed'), ['message' : message], FailureHandling.STOP_ON_FAILURE)
		} else {			
			WebUI.callTestCase(findTestCase('Test Cases/pages/auth/login/verify toast failed login displayed'), [:], FailureHandling.STOP_ON_FAILURE)
		}
	}

	
}