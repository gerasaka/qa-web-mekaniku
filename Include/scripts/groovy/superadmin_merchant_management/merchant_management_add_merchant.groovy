package superadmin_merchant_management
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class merchant_management_add_merchant {
	@When("I click add merchant")
	def I_click_add_merchant() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_management/i_click_add_merchant'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type owner name {string} in add merchant")
	def I_type_owner_name_in_add_merchant(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_modal/i_type_owner_name_in_add_merchant'), [('fullname'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type email {string} in add merchant")
	def I_type_email_in_add_merchant(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_modal/i_type_email_in_add_merchant'), [('email'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I type phone {string} in add merchant")
	def I_type_phone_in_add_merchant(String value) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_modal/i_type_phone_in_add_merchant'), [('phone'):value], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click submit add merchant")
	def I_click_submit_add_merchant() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_modal/i_click_submit_add_merchant'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify add merchant modal")
	def I_verify_add_merchant_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_modal/i_verify_add_merchant_modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify success add merchant modal")
	def I_verify_success_add_merchant_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/superadmin_merchant_add_success_modal/i_verify_add_success_merchant_modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}