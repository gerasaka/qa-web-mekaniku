package general
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class GeneralSteps {
	@Given("I open app")
	def I_open_app() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/general/open app'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I click cancel button")
	def I_click_cancel_in_modal() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/general/click cancel in modal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify delete modal displayed")
	def I_verify_delete_modal_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/product/verify product modal displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I verify success toast displayed")
	def I_verify_success_toast_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/general/verify success toast displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("I verify danger toast displayed")
	def I_verify_danger_toast_displayed() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/general/verify danger toast displayed'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}