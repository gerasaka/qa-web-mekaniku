package profile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditDescriptionSteps {
	@Given("I open profile page")
	def I_open_profile_page() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/open profile page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I click edit description button")
	def I_click_edit_description_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/edit description/click edit description buton'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I input new (.*)")
	def I_input_new_description(String desc) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/edit description/input description'), ['desc' : desc], FailureHandling.STOP_ON_FAILURE)
	}

	@And("I click save description button")
	def I_click_save_description_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/edit description/click save description button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("I verify workshop (.*) changed")
	def I_verify_workshop_description_changed(String desc) {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/edit description/verify description change'), ['desc' : desc], FailureHandling.STOP_ON_FAILURE)
	}
}