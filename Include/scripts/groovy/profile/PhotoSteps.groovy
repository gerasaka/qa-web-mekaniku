package profile
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class PhotoSteps {
	@When("I select workshop photo")
	def I_select_workshop_photo() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/photo/select workshop photo'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("I select big workshops photo")
	def I_select_big_workshops_photo() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/photo/select big workshops photo'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click delete first workshop photo")
	def I_click_delete_first_workshop_photo() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/photo/click delete first workshop photo button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I click delete photo button")
	def I_click_delete_photo_button() {
		WebUI.callTestCase(findTestCase('Test Cases/pages/profile/photo/click delete photo button'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}